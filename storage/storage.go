package storage

import (
	"io"
	"io/ioutil"

	"bitbucket.org/eithz/fkeeper/config"
)

func saveFileDstDir(id string, filename string, dstDir string, config config.Config) {
	serialChannel <- indexUnit{bucket: id + "/" + filename, dstDir: dstDir}
	go FlushIndex(config)
}

func getFileDstDir(id string, filename string) string {
	return index[id+"/"+filename]
}

func StoreFile(config config.Config, name string, input io.Reader) (*string, error) {
	name = sanitizeName(name)
	data, err := ioutil.ReadAll(input)
	if err != nil {
		return nil, err
	}
	id, dirPath := dirPathFromData(data)
	dstDir := config["dataDir"] + dirPath
	if err := ensureDirectory(dstDir); err != nil {
		return nil, err
	}
	if err = ioutil.WriteFile(dstDir+"/"+name, data, 0755); err != nil {
		return nil, err
	}
	path := id + "/" + name
	saveFileDstDir(id, name, dstDir, config)
	return &path, nil
}

func GetFile(id string, filename string) ([]byte, error) {
	dstDir := getFileDstDir(id, filename)
	return ioutil.ReadFile(dstDir + "/" + filename)
}
