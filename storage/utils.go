package storage

import (
	"bytes"
	"crypto/md5"
	"fmt"
	"os"
)

func sanitizeName(name string) string {
	return name
}
func dirPathFromData(data []byte) (string, string) {
	hash := md5.Sum(data)
	var buffer bytes.Buffer
	var id bytes.Buffer
	for idx, octet := range hash {
		buffer.WriteString("/")
		buffer.WriteString(fmt.Sprintf("%x", octet))
		if idx < 2 || idx == len(hash)-1 {
			id.WriteString(fmt.Sprintf("%x", octet))
		}
	}
	return id.String(), buffer.String()
}

func ensureDirectory(path string) error {
	return os.MkdirAll(path, 0755)
}
