package storage

import (
	"bytes"
	"encoding/gob"
	"io/ioutil"
	"os"

	"bitbucket.org/eithz/fkeeper/config"

	log "github.com/Sirupsen/logrus"
)

var serialChannel = make(chan indexUnit, 5)
var index = make(map[string]string)
var indexFlushLock = make(chan struct{}, 1)

func init() {
	go serialWriter()
	indexFlushLock <- struct{}{}
}

func serialWriter() {
	for {
		select {
		case update := <-serialChannel:
			index[update.bucket] = update.dstDir
			log.WithFields(log.Fields{"index": index}).Debug("Paths updated")
		}
	}
}

type indexUnit struct {
	bucket string
	dstDir string
}

func FlushIndex(config config.Config) error {
	log.Debug("Index flush begin")
	<-indexFlushLock
	defer func() { indexFlushLock <- struct{}{} }()
	var indexContent bytes.Buffer
	encoder := gob.NewEncoder(&indexContent)
	if err := encoder.Encode(index); err != nil {
		log.WithFields(log.Fields{"error": err.Error()}).Error("Failed to update index")
		return err
	}
	indexFilePath := config["dataDir"] + "/" + "index"
	ioutil.WriteFile(indexFilePath, indexContent.Bytes(), 0755)
	log.Debug("Index flushed!")
	return nil
}

// RestoreIndex restores current index from flushed. Concurrent unsafe.
func RestoreIndex(config config.Config) error {
	indexFilePath := config["dataDir"] + "/" + "index"
	data, err := os.Open(indexFilePath)
	if err != nil {
		return err
	}
	dec := gob.NewDecoder(data)
	dec.Decode(&index)
	return nil
}
