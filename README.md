# Usage

```
fkeeper -data-dir="/var/tmp/storage" -addr=:8080
```

Upload
```
~  curl -T pic.jpg 127.0.0.1:8000
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 1260k  100    36  100 1260k   1509  51.5M --:--:-- --:--:-- --:--:-- 53.5M
http://127.0.0.1:8000/a67267/pic.jpg%
```

Download
```
~  curl http://127.0.0.1:8000/a67267/pic.jpg > /dev/null
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100 1260k    0 1260k    0     0   252M      0 --:--:-- --:--:-- --:--:--  307M
```
