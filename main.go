package main

import (
	"flag"

	"bitbucket.org/eithz/fkeeper/application"
	"bitbucket.org/eithz/fkeeper/config"

	log "github.com/Sirupsen/logrus"
)

func initLogging() {
	log.SetLevel(log.DebugLevel)
	log.SetFormatter(&log.JSONFormatter{})
}

func init() {
	initLogging()
}

func main() {
	var addr = flag.String("addr", ":8000", "Listen address in <ip>:<port> format")
	var dataDir = flag.String("data-dir", "/tmp", "Where store files")
	var url = flag.String("url", "http://127.0.0.1:8000", "Domain used for generating urls")
	flag.Parse()
	app := application.NewApplication(config.Config{"addr": *addr, "dataDir": *dataDir, "url": *url})
	if err := app.Run(); err != nil {
		log.WithFields(log.Fields{"error": err.Error()}).Fatal("Can't start")
	}
}
