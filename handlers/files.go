package handlers

import (
	"mime"
	"net/http"

	"bitbucket.org/eithz/fkeeper/config"
	"bitbucket.org/eithz/fkeeper/storage"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

func handleError(err error, w http.ResponseWriter) {
	http.Error(w, err.Error(), 502)
}

//NewFileUploader creates handler which uploads files
func NewFileUploader(config config.Config) http.HandlerFunc {
	log.WithFields(log.Fields{"config": config}).Debug("FileUploader created")
	return func(w http.ResponseWriter, r *http.Request) {
		cType, _, _ := mime.ParseMediaType(r.Header.Get("Content-Type"))
		if cType == "multipart/form-data" {
			http.Error(w, "Not implemented", 400)
		} else {
			vars := mux.Vars(r)
			path, err := storage.StoreFile(config, vars["filename"], r.Body)
			if err != nil {
				handleError(err, w)
				return
			}
			w.Write([]byte(config["url"] + "/" + *path))
		}
	}
}

//NewFileDownloader creates handler which downloads files
func NewFileDownloader(config config.Config) http.HandlerFunc {
	log.WithFields(log.Fields{"config": config}).Debug("FileDownloader created")
	return func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		filename := vars["filename"]
		id := vars["id"]

		data, err := storage.GetFile(id, filename)
		if err != nil {
			handleError(err, w)
			return
		}
		w.Write(data)
	}
}
