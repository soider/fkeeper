package application

import (
	"net/http"
	"os"
	"os/signal"

	"bitbucket.org/eithz/fkeeper/config"
	"bitbucket.org/eithz/fkeeper/handlers"
	"bitbucket.org/eithz/fkeeper/storage"

	log "github.com/Sirupsen/logrus"
	"github.com/gorilla/mux"
)

//
type App struct {
	config  config.Config
	handler *mux.Router
	server  *http.Server
}

func (a *App) Run() error {
	log.Debug("Preparing to run")
	return a.server.ListenAndServe()
}

func (a *App) Stop() {
	log.Debug("Stopping")
	storage.FlushIndex(a.config)
	os.Exit(0)
}

// Register Interruption signal
func (a *App) registerHandlers() {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	go func() {
		<-c
		a.Stop()
	}()
}

func (a *App) init() {
	a.registerHandlers()
	a.handler = mux.NewRouter()
	a.handler.HandleFunc("/{filename}", handlers.NewFileUploader(a.config))
	a.handler.HandleFunc("/{id}/{filename}", handlers.NewFileDownloader(a.config))
	a.server = &http.Server{Addr: a.config["addr"], Handler: a.handler}
	storage.RestoreIndex(a.config)
}

func NewApplication(config config.Config) *App {
	app := &App{config: config}
	app.init()
	return app
}
